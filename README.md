# git-archive-in-CI

[![pipeline status](https://gitlab.com/gitlablab/git-archive-in-ci/badges/master/pipeline.svg)](https://gitlab.com/gitlablab/git-archive-in-ci/commits/master)

Explores using `git archive` as part of GitLab's CI/CD
