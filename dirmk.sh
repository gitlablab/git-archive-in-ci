#!/usr/bin/sh

RANDOM=7

dirmk()
{
  local k n
  n=$(( $1 % 3 ))
  if [[ $n -eq 0 ]] ; then touch leaf ; fi
  for ((k=0;k<$n;++k))
    do mkdir mydir$k
    cd mydir$k
    dirmk $RANDOM
    cd ..
  done
}

dirmk $RANDOM

find . -type d -name 'mydir*' -execdir tree -J -D -o tree.json \;
find . -type d -name 'mydir*' -execdir tree -J -D -o tree.json \;

